# README #

### Что вам понадобится ###

* Microsoft Visual Studio (У меня Community 2013. Она, если что, беcплатная.)
* HLDS-сервер, желательно новой версии (Я использую 6153-й билд.)

### С чего начать? ###
* Для начала откройте в Visual Studio диспетчер свойств, и в свойствах WinDebug измените пользовательский макрос HLDS_DIR на путь к вашей директории HLDS.

### Что использовано в качестве основы? ###
Проект был начат как форк этого кода  https://code.google.com/p/cs16nd/
Так же иногда используется код из https://github.com/Arkshine/CSSDK

### Что ещё? ###
* При компиляции под Linux разрешить выполнение ./BuildVersioning.sh

### Статус Мастер-ветки ###
* Linux
* [![Build Status](https://travis-ci.org/Chuvi-w/CS_GameDLL.svg?branch=master)](https://travis-ci.org/Chuvi-w/CS_GameDLL)
* 
* Windows
* [![Build status](https://ci.appveyor.com/api/projects/status/4u8etavu5q4tta2r/branch/master?svg=true)](https://ci.appveyor.com/project/Chuvi-w/cs-gamedll/branch/master)
