#
# Half-Life Full SDK 2.3 hl_i386.so Makefile for x86 Linux
#
# October 2002 by Leon Hartwig (hartwig@valvesoftware.com)
#

DLLNAME=cs

ARCH=i386

#make sure this is the correct compiler for your system
CC=gcc-4.8
CXX=g++-4.8
DLL_SRCDIR=./dlls
ENGINE_SRCDIR=./engine
COMMON_SRCDIR=./common
WPN_SHARED_SRCDIR=./dlls
PM_SHARED_SRCDIR=./pm_shared
GAME_SHARED_SRCDIR=./game_shared
PUBLIC_SRCDIR=./public
TIER1_SRCDIR=$(PUBLIC_SRCDIR)/tier1
PCH_HEADER_H_NAME=stdafx.h
PCH_HEADER_H_FILE=$(DLL_SRCDIR)/$(PCH_HEADER_H_NAME)


BASE_CFLAGS=	-Dstricmp=strcasecmp -D_strnicmp=strncasecmp -Dstrnicmp=strncasecmp \
		-DCLIENT_WEAPONS -DLINUX -w -m32 -fpermissive -std=c++11

#safe optimization
OPT_FLAGS=$(BASE_CFLAGS) -O1  -fno-strict-aliasing -DNDEBUG

#full optimization
#CFLAGS=$(BASE_CFLAGS) -w -O1 -m486 -ffast-math -funroll-loops \
	-fomit-frame-pointer -fexpensive-optimizations \
	-malign-loops=2 -malign-jumps=2 -malign-functions=2

#use these when debugging 
DEBUG_FLAGS =$(BASE_CFLAGS) -g -ggdb3 -D_DEBUG
OUTPUT_DIR = ./Bin/Linux
TEMP_DIR = ./Bin/Linux/Temp

ifeq "$(DEBUG)" "true"
	PRE_BIN_DIR = Debug
	PRE_CFLAGS = $(DEBUG_FLAGS)
else
	PRE_BIN_DIR = Release
	PRE_CFLAGS = $(OPT_FLAGS)
endif

ifeq "$(BINLOG)" "true"
	BIN_DIR = $(PRE_BIN_DIR)_Binlog
	CFLAGS = $(PRE_CFLAGS) -DBINLOG_ENABLED
else
	BIN_DIR = $(PRE_BIN_DIR)
	CFLAGS = $(PRE_CFLAGS)
endif

DLL_OBJDIR=$(TEMP_DIR)/$(BIN_DIR)/$(DLL_SRCDIR)
WPN_SHARED_OBJDIR=$(TEMP_DIR)/$(BIN_DIR)/$(WPN_SHARED_SRCDIR)
PM_SHARED_OBJDIR=$(TEMP_DIR)/$(BIN_DIR)/$(PM_SHARED_SRCDIR)
GAME_SHARED_OBJDIR=$(TEMP_DIR)/$(BIN_DIR)/$(GAME_SHARED_SRCDIR)
PCH_HEADER_DIR=$(TEMP_DIR)/$(BIN_DIR)
PCH_HEADER=$(PCH_HEADER_DIR)/$(PCH_HEADER_H_NAME).gch
INCLUDEDIRS=-I. -I$(ENGINE_SRCDIR) -I$(COMMON_SRCDIR) -I$(PM_SHARED_SRCDIR) -I$(GAME_SHARED_SRCDIR) -I$(PUBLIC_SRCDIR) -I$(TIER1_SRCDIR) -I$(DLL_SRCDIR)

LDFLAGS=

SHLIBEXT=so
SHLIBCFLAGS=-fPIC
SHLIBLDFLAGS=-shared

DO_CC=$(CC) $(CFLAGS) $(SHLIBCFLAGS) $(INCLUDEDIRS) -o $@ -c $<
DO_CXX=$(CXX) $(CFLAGS) $(SHLIBCFLAGS) -I$(PCH_HEADER_DIR) $(INCLUDEDIRS) -o $@ -c $<
#############################################################################
# SETUP AND BUILD
# GAME
#############################################################################

$(DLL_OBJDIR)/%.o: $(DLL_SRCDIR)/%.cpp
	$(DO_CXX)

$(WPN_SHARED_OBJDIR)/%.o: $(WPN_SHARED_SRCDIR)/%.cpp
	$(DO_CXX)

$(GAME_SHARED_OBJDIR)/%.o: $(GAME_SHARED_SRCDIR)/%.cpp
	$(DO_CXX)

$(PM_SHARED_OBJDIR)/%.o: $(PM_SHARED_SRCDIR)/%.c
	$(DO_CC)

OBJ = \
	$(DLL_OBJDIR)/airtank.o \
	$(DLL_OBJDIR)/ammo.o \
	$(DLL_OBJDIR)/animating.o \
	$(DLL_OBJDIR)/animation.o \
	$(DLL_OBJDIR)/bmodels.o \
	$(DLL_OBJDIR)/buttons.o \
	$(DLL_OBJDIR)/cbase.o \
	$(DLL_OBJDIR)/client.o \
	$(DLL_OBJDIR)/combat.o \
	$(DLL_OBJDIR)/doors.o \
	$(DLL_OBJDIR)/effects.o \
	$(DLL_OBJDIR)/explode.o \
	$(DLL_OBJDIR)/func_break.o \
	$(DLL_OBJDIR)/func_tank.o \
	$(DLL_OBJDIR)/game.o \
	$(DLL_OBJDIR)/gamerules.o \
	$(DLL_OBJDIR)/ggrenade.o \
	$(DLL_OBJDIR)/globals.o \
	$(DLL_OBJDIR)/healthkit.o \
	$(DLL_OBJDIR)/hintmessage.o \
	$(DLL_OBJDIR)/hostage.o \
	$(DLL_OBJDIR)/hostage_localnav.o \
	$(DLL_OBJDIR)/h_ai.o \
	$(DLL_OBJDIR)/h_battery.o \
	$(DLL_OBJDIR)/h_cycler.o \
	$(DLL_OBJDIR)/h_export.o \
	$(DLL_OBJDIR)/items.o \
	$(DLL_OBJDIR)/lights.o \
	$(DLL_OBJDIR)/mapinfo.o \
	$(DLL_OBJDIR)/maprules.o \
	$(DLL_OBJDIR)/MemPool.o \
	$(DLL_OBJDIR)/mortar.o \
	$(DLL_OBJDIR)/mpstubb.o \
	$(DLL_OBJDIR)/multiplay_gamerules.o \
	$(DLL_OBJDIR)/observer.o \
	$(DLL_OBJDIR)/pathcorner.o \
	$(DLL_OBJDIR)/perf_counter.o \
	$(DLL_OBJDIR)/plane.o \
	$(DLL_OBJDIR)/plats.o \
	$(DLL_OBJDIR)/player.o \
	$(DLL_OBJDIR)/singleplay_gamerules.o \
	$(DLL_OBJDIR)/skill.o \
	$(DLL_OBJDIR)/sound.o \
	$(DLL_OBJDIR)/soundent.o \
	$(DLL_OBJDIR)/spectator.o \
	$(DLL_OBJDIR)/subs.o \
	$(DLL_OBJDIR)/training_gamerules.o \
	$(DLL_OBJDIR)/triggers.o \
	$(DLL_OBJDIR)/util.o \
	$(DLL_OBJDIR)/vehicle.o \
	$(DLL_OBJDIR)/weapons.o \
	$(DLL_OBJDIR)/world.o \
	$(DLL_OBJDIR)/BinLog.o \
	$(DLL_OBJDIR)/BuildInfo.o\
	$(WPN_SHARED_OBJDIR)/wpn_ak47.o \
	$(WPN_SHARED_OBJDIR)/wpn_aug.o \
	$(WPN_SHARED_OBJDIR)/wpn_awp.o \
	$(WPN_SHARED_OBJDIR)/wpn_c4.o \
	$(WPN_SHARED_OBJDIR)/wpn_deagle.o \
	$(WPN_SHARED_OBJDIR)/wpn_elite.o \
	$(WPN_SHARED_OBJDIR)/wpn_famas.o \
	$(WPN_SHARED_OBJDIR)/wpn_fiveseven.o \
	$(WPN_SHARED_OBJDIR)/wpn_flashbang.o \
	$(WPN_SHARED_OBJDIR)/wpn_g3sg1.o \
	$(WPN_SHARED_OBJDIR)/wpn_galil.o \
	$(WPN_SHARED_OBJDIR)/wpn_glock18.o \
	$(WPN_SHARED_OBJDIR)/wpn_hegrenade.o \
	$(WPN_SHARED_OBJDIR)/wpn_knife.o \
	$(WPN_SHARED_OBJDIR)/wpn_m249.o \
	$(WPN_SHARED_OBJDIR)/wpn_m3.o \
	$(WPN_SHARED_OBJDIR)/wpn_m4a1.o \
	$(WPN_SHARED_OBJDIR)/wpn_mac10.o \
	$(WPN_SHARED_OBJDIR)/wpn_mp5navy.o \
	$(WPN_SHARED_OBJDIR)/wpn_p228.o \
	$(WPN_SHARED_OBJDIR)/wpn_p90.o \
	$(WPN_SHARED_OBJDIR)/wpn_scout.o \
	$(WPN_SHARED_OBJDIR)/wpn_sg550.o \
	$(WPN_SHARED_OBJDIR)/wpn_sg552.o \
	$(WPN_SHARED_OBJDIR)/wpn_smokegrenade.o \
	$(WPN_SHARED_OBJDIR)/wpn_tmp.o \
	$(WPN_SHARED_OBJDIR)/wpn_ump45.o \
	$(WPN_SHARED_OBJDIR)/wpn_usp.o \
	$(WPN_SHARED_OBJDIR)/wpn_xm1014.o \
	$(GAME_SHARED_OBJDIR)/voice_gamemgr.o \
	$(PM_SHARED_OBJDIR)/pm_debug.o \
	$(PM_SHARED_OBJDIR)/pm_math.o \
	$(PM_SHARED_OBJDIR)/pm_shared.o



$(OUTPUT_DIR)/$(BIN_DIR)/$(DLLNAME).$(SHLIBEXT) : neat $(PCH_HEADER) $(OBJ)
	$(CC) $(CFLAGS) $(SHLIBLDFLAGS) $(LDFLAGS) -o $@ $(OBJ)

$(PCH_HEADER):
	$(CXX) $(CFLAGS) $(SHLIBCFLAGS) $(INCLUDEDIRS) -x c++-header $(PCH_HEADER_H_FILE) -o $(PCH_HEADER)

neat:
	-./BuildVersioning.sh
	-mkdir -p $(DLL_OBJDIR)
	#-mkdir -p $(WPN_SHARED_OBJDIR)
	-mkdir -p $(GAME_SHARED_OBJDIR)
	-mkdir -p $(PM_SHARED_OBJDIR)
	-mkdir -p $(OUTPUT_DIR)/$(BIN_DIR)
clean:
	-rm -rf $(OUTPUT_DIR)
debug:
	$(MAKE)  DEBUG=true BINLOG=false
release:
	$(MAKE)  DEBUG=false BINLOG=false
debug_binlog:
	$(MAKE)  DEBUG=true BINLOG=true
release_binlog:
	$(MAKE)  DEBUG=false BINLOG=true
all: debug release debug_binlog release_binlog

default: release
