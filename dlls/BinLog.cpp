#include "stdafx.h"
#ifdef BINLOG_ENABLED
#include <stdio.h>
#include <time.h>
#ifdef WIN32
#include <direct.h>

#else
#include <sys/stat.h>
#include <linux/limits.h>
#include <dlfcn.h>
#endif
#include "enginecallback.h"

FILE *BinLogFile = NULL;
char BinLogFileName[255];

void CreateBinLog()
{
	if (BinLogFile)
		return;
#ifdef WIN32
	_mkdir("./cstrike/MPDLL_BINLOG");
#else
	mkdir("./cstrike/MPDLL_BINLOG",0777);
#endif
	time_t rawtime;
	struct tm * t;

	time(&rawtime);
	t = localtime(&rawtime);
	static char BinLogName[MAX_PATH];
	sprintf(BinLogName, "./cstrike/MPDLL_BINLOG/%i_%i_%i__%i_%i_%i.log", 1900 + t->tm_year, t->tm_mon, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);
	BinLogFile = fopen(BinLogName, "wb");
	if (!BinLogFile)
		return;
	int BaseAddr;
	int ThisAddr;
#ifdef WIN32
	BaseAddr = reinterpret_cast<int>(GetModuleHandle("mp.dll"));
#else
	BaseAddr = reinterpret_cast<int>(dlopen("cs.so",RTLD_LAZY));
	if (BaseAddr)
	{
		dlclose(BaseAddr);
	}
#endif
	ThisAddr = reinterpret_cast<int>(&CreateBinLog);
	strcpy(BinLogFileName, BinLogName);

	fwrite(&BaseAddr, sizeof(int), 1, BinLogFile);
	fwrite(&ThisAddr, sizeof(int), 1, BinLogFile);
}



#define CALLFUNC_MAXLEN 90
void  WriteLog(const char *Callfunc, void *Caller)
{
	if (!BinLogFile)
		CreateBinLog();
	if (!BinLogFile)
		return;
	if (ftell(BinLogFile) > 50 * 1024 * 1024)
	{
		fclose(BinLogFile);
		BinLogFile = NULL;
		remove(BinLogFileName);
		CreateBinLog();
		if (!BinLogFile)
			return;
	}

	static char CallFuncName[CALLFUNC_MAXLEN];
	int CallFuncNameLen = strlen(Callfunc)+1;
	if (CallFuncNameLen > CALLFUNC_MAXLEN)
	{
		CallFuncNameLen = CALLFUNC_MAXLEN;
	}
	memcpy(CallFuncName, Callfunc, CallFuncNameLen);
	CallFuncName[CallFuncNameLen - 1] = 0;
	//int CallfuncAddr = reinterpret_cast<int>(Callfunc);
	int CallerAddr = reinterpret_cast<int>(Caller);
	float CurTime = g_engfuncs.pfnTime?GAME_TIME():0.0;
	fwrite(&CurTime, sizeof(CurTime), 1, BinLogFile);
	fwrite(&CallFuncNameLen, 1, 1, BinLogFile);
	fwrite(&CallFuncName, CallFuncNameLen, 1, BinLogFile);
	fwrite(&CallerAddr, sizeof(int), 1, BinLogFile);
	fflush(BinLogFile);
}
#endif