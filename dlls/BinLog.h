#ifndef BinLog_h__
#define BinLog_h__
// __builtin_return_address linux

#ifdef BINLOG_ENABLED
#ifdef __cplusplus
extern "C"
#else
extern
#endif
void WriteLog(const char *Callfunc, void *Caller);

#ifdef WIN32
#ifdef __cplusplus
extern "C"
#endif
void * _ReturnAddress(void);
#pragma intrinsic(_ReturnAddress)

#define BINTRACE WriteLog((const char*)__FUNCTION__,_ReturnAddress());
#else
#define BINTRACE WriteLog((const char*)__FUNCTION__, __builtin_return_address(0));
#endif
#else
#define BINTRACE
#endif
//#error TEST
#endif // BinLog_h__