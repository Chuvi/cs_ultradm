﻿#include "stdafx.h"
#include "cbase.h"
#include "BuildInfo.h"
#include "util.h"
#include "gamerules.h"
#include "client.h"


extern "C" EXPORT const char * BuildGitBranch = GIT_BRANCH;
extern "C" EXPORT const char * BuildGitCommitHash = GIT_COMMIT_HASH;
extern "C" EXPORT const char * BuildGitPullRequestNumber = GIT_PULL_REQUEST_NUMBER;
extern "C" EXPORT const char * BuildGitCommitAuthor = GIT_COMMIT_AUTHOR;
extern "C" EXPORT const char * BuildDate = __DATE__;
extern "C" EXPORT const char * BuildTime = __TIME__;
extern "C" EXPORT const char * BuildConfiguration = BUILD_CONFIGURATION;
extern int gmsgMOTD;

void PrintBuildInfo(edict_t *ed)
{
	if (!ed)
		return;
	static char MsgBuf[128];
	snprintf(MsgBuf, sizeof(MsgBuf) - 1, "GameDLL info:\n\tBranch: \"%s\"\n", BuildGitBranch);
	CLIENT_PRINTF(ed, print_console, MsgBuf);

	snprintf(MsgBuf, sizeof(MsgBuf) - 1, "\tCommit hash: \"%s\"\n", BuildGitCommitHash);
	CLIENT_PRINTF(ed, print_console, MsgBuf);
	
	//snprintf(MsgBuf, sizeof(MsgBuf) - 1, "\tCommit author: \"%s\"\n", BuildGitCommitAuthor);
	//CLIENT_PRINTF(ed, print_console, MsgBuf);
	if (BuildGitPullRequestNumber[0] != '\0')
	{
		snprintf(MsgBuf, sizeof(MsgBuf) - 1, "\tPull request number: \"%s\\n", BuildGitPullRequestNumber);
		CLIENT_PRINTF(ed, print_console, MsgBuf);
	}

	snprintf(MsgBuf, sizeof(MsgBuf) - 1, "\tDLL Build: %s %s\n\n", BuildDate, BuildTime);
	CLIENT_PRINTF(ed, print_console, MsgBuf);
}

void PrintBuildInfoSrv()
{
	if (!g_engfuncs.pfnServerPrint)
		return;

	static char MsgBuf[128];
	snprintf(MsgBuf, sizeof(MsgBuf) - 1, "GameDLL info:\n\tBranch: \"%s\"\n", BuildGitBranch);
	g_engfuncs.pfnServerPrint(MsgBuf);

	snprintf(MsgBuf, sizeof(MsgBuf) - 1, "\tCommit hash: \"%s\"\n", BuildGitCommitHash);
	g_engfuncs.pfnServerPrint(MsgBuf);

	//snprintf(MsgBuf, sizeof(MsgBuf) - 1, "\tCommit author: \"%s\"\n", BuildGitCommitAuthor);
	//CLIENT_PRINTF(ed, print_console, MsgBuf);
	if (BuildGitPullRequestNumber[0] != '\0')
	{
		snprintf(MsgBuf, sizeof(MsgBuf) - 1, "\tPull request number: \"%s\\n", BuildGitPullRequestNumber);
		g_engfuncs.pfnServerPrint(MsgBuf);
	}

	snprintf(MsgBuf, sizeof(MsgBuf) - 1, "\tDLL Build: %s %s\n\n", BuildDate, BuildTime);
	g_engfuncs.pfnServerPrint(MsgBuf);
}

BOOL SendUrlMOTD(edict_t *client,const char *URL)
{
	BINTRACE
	int char_count = 0;
	char Msg[MAX_MOTD_LENGTH+1];
	strncpy(Msg, URL, MAX_MOTD_LENGTH);
	Msg[MAX_MOTD_LENGTH] = '\0';
	char *pMSG = Msg;
	char chunk[MAX_MOTD_CHUNK + 1];
	while (pMSG && *pMSG && char_count < MAX_MOTD_LENGTH)
	{
		

		if (strlen(pMSG) < MAX_MOTD_CHUNK)
		{
			strcpy(chunk, pMSG);
		}
		else
		{
			strncpy(chunk, pMSG, MAX_MOTD_CHUNK);
			chunk[MAX_MOTD_CHUNK] = '\0';
		}

		char_count += strlen(chunk);

		if (char_count < MAX_MOTD_LENGTH)
			pMSG += strlen(chunk);
		else
			*pMSG = 0;

		MESSAGE_BEGIN(client?MSG_ONE:MSG_ALL, gmsgMOTD, NULL, client);
		WRITE_BYTE(*pMSG ? FALSE : TRUE);
		WRITE_STRING(chunk);
		MESSAGE_END();
	}
	
	return TRUE;
}

void UpdateGameDLL()
{
	SendUrlMOTD(0, "http:////chuvi-w.github.io//CS_GameDLL//MOTD_DLLUpdate.html");
	g_pGameRules->SetRestartServerAtRoundEnd();
	ALERT(at_logged, "UpdateGameDLL: Server will shut down at round end\n");
}
