#ifndef BuildInfo_h__
#define BuildInfo_h__

#define BUILD_DATE __DATE__ 
#define BUILD_TIME __TIME__ 
#ifdef _DEBUG 
#ifdef BINLOG_ENABLED 
#define BUILD_CONFIGURATION "Debug_Binlog"
#else
#define BUILD_CONFIGURATION "Debug"
#endif
#else 
#ifdef BINLOG_ENABLED 
#define BUILD_CONFIGURATION "Release_Binlog"
#else
#define BUILD_CONFIGURATION "Release"
#endif
#endif 

#include "../BuildVersion.h"

void PrintBuildInfo(edict_t *ed);
BOOL SendUrlMOTD(edict_t *client, const char *URL);
void UpdateGameDLL();
void PrintBuildInfoSrv();
#endif // BuildInfo_h__
