#!/bin/bash
#Определяем место, откуда запущен скрипт.
ABSOLUTE_FILENAME=`readlink -e "$0"`
path=`dirname "$ABSOLUTE_FILENAME"`
OutFile="$path/BuildVersion.h"
GIT_BRANCH="Unknown"
GIT_COMMIT_HASH="Unknown"
GIT_COMMIT_AUTHOR="Unknown"
GIT_PULL_REQUEST_NUMBER=
 

function PrintOutput
{
	if [ -f "$OutFile" ] ; 
	then
		rm "$OutFile"
	fi
	echo '#ifndef _BUILD_VERSION_H' >>"$OutFile"
	echo '#define _BUILD_VERSION_H' >>"$OutFile"
	echo '#define GIT_BRANCH  "'$GIT_BRANCH'"' >>"$OutFile"
	echo '#define GIT_COMMIT_HASH  "'$GIT_COMMIT_HASH'"' >>"$OutFile"
	echo '#define GIT_PULL_REQUEST_NUMBER  "'$GIT_PULL_REQUEST_NUMBER'"' >>"$OutFile"
	echo '#define GIT_COMMIT_AUTHOR  "'$GIT_COMMIT_AUTHOR'"' >>"$OutFile"
	echo '#endif _BUILD_VERSION_H' >>"$OutFile"
}



function GetCommitHash
{
	if [ -z "$TRAVIS_COMMIT" ]; 
	then
		TmpVar=$(git rev-parse  HEAD 2>/dev/null)
		if [ $? -eq 0 ];
		then
		GIT_COMMIT_HASH=$TmpVar
		fi
		
	else
		GIT_COMMIT_HASH=$TRAVIS_COMMIT
	fi
}

function GetBranchName
{
	if [ -z "$TRAVIS_BRANCH" ]; 
	then
		TmpVar=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)
		if [ $? -eq 0 ];
		then
			GIT_BRANCH=$TmpVar
		fi
		
	else
		GIT_BRANCH=$TRAVIS_BRANCH
	fi
}



function GetCommitAuthor
{
	TmpVar=$(git log --pretty="%an" -1  2>/dev/null)
	if [ $? -eq 0 ];
	then
		GIT_COMMIT_AUTHOR=$TmpVar
	fi
}

function GetPullNumber
{
	if [ -z "$TRAVIS_PULL_REQUEST" ]; 
	then
		GIT_PULL_REQUEST_NUMBER=""	
	else
		if [[ "$TRAVIS_PULL_REQUEST" -eq "false" ]]
		then
			GIT_PULL_REQUEST_NUMBER=""
		else
			GIT_PULL_REQUEST_NUMBER=$TRAVIS_PULL_REQUEST
		fi
	fi
}
GetBranchName
GetCommitHash
GetCommitAuthor
GetPullNumber
PrintOutput
exit 0
