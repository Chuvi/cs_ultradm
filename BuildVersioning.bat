@echo off
setlocal enabledelayedexpansion
set CUR=%~dp0
set FILE="%CUR%BuildVersion.h"
set TMP="%CUR%BuildVersion.tmp"
set /a GIT_EXIST = 0
set GIT_BRANCH=git not found
set GIT_COMMIT_HASH=git not found
set GIT_COMMIT_AUTHOR=git not found
set GIT_PULL_REQUEST_NUMBER=
set /a RESULT = 0
%~d0
cd %CUR%

call :CheckGit
call :GetBranchName
call :GetCommitHash
call :GetCommitAuthor
call :GetPullNumberAppveyor
call :MakeVersionFile
if EXIST %TMP% (del %TMP%)
goto EXIT



	
:CheckGit
set /a GIT_EXIST = 0
git>>%TMP%
rem git without params should return 1 in errorlevel
if %errorlevel% == 1 (set /a GIT_EXIST = 1)
exit /b

:GetBranchName
	call :GetBranchNameAppveyor
	if %RESULT%==0 (call :GetBranchNameGit)
	exit /b

:GetBranchNameGit
	if %GIT_EXIST% == 0 (exit /b)
	git rev-parse --abbrev-ref HEAD >%TMP%
	if %errorlevel% == 0 (set /p GIT_BRANCH=<%TMP%)
	exit /b
	
:GetBranchNameAppveyor
	set /a RESULT = 0
	if  defined APPVEYOR_REPO_BRANCH ( 
		set /p GIT_BRANCH=%APPVEYOR_REPO_BRANCH%
		set /a RESULT = 1)
	exit /b
	
:GetCommitHash
	call :GetCommitHashAppveyor
	if %RESULT%==0 (call :GetCommitHashGit)
	exit /b
	
:GetCommitHashGit
	if %GIT_EXIST% == 0 (exit /b)
	git rev-parse  HEAD >%TMP%
	if %errorlevel% == 0 (set /p GIT_COMMIT_HASH=<%TMP%)
	exit /b

:GetCommitHashAppveyor
	set /a RESULT = 0
	if  defined APPVEYOR_REPO_COMMIT ( 
		set /p GIT_COMMIT_HASH=%APPVEYOR_REPO_COMMIT%
		set /a RESULT = 1)
	exit /b

:GetCommitAuthor
	call :GetCommitAuthorAppveyor
	if %RESULT%==0 (call :GetCommitAuthorGit)
	exit /b
	
:GetCommitAuthorGit
	if %GIT_EXIST% == 0 (exit /b)
	git log --pretty="%%an" -1 >%TMP%
	if %errorlevel% == 0 (set  /p GIT_COMMIT_AUTHOR=<%TMP%)
	exit /b

:GetCommitAuthorAppveyor
	set /a RESULT = 0
	if  defined APPVEYOR_REPO_COMMIT_AUTHOR ( 
		set /p GIT_COMMIT_AUTHOR=%APPVEYOR_REPO_COMMIT_AUTHOR%
		set /a RESULT = 1)
	exit /b
	
	
:GetPullNumberAppveyor
	set /a RESULT = 0
	if  defined APPVEYOR_PULL_REQUEST_NUMBER ( 
		set /p GIT_PULL_REQUEST_NUMBER=%APPVEYOR_PULL_REQUEST_NUMBER%
		set /a RESULT = 1)
	exit /b
	
:MakeVersionFile
if EXIST %FILE% (del %FILE%)
echo #ifndef _BUILD_VERSION_H >>%FILE%
echo #define _BUILD_VERSION_H >>%FILE%
echo #define GIT_BRANCH  "%GIT_BRANCH%" >>%FILE%
echo #define GIT_COMMIT_HASH  "%GIT_COMMIT_HASH%" >>%FILE%
echo #define GIT_PULL_REQUEST_NUMBER  "%GIT_PULL_REQUEST_NUMBER%" >>%FILE%
echo #define GIT_COMMIT_AUTHOR  "%GIT_COMMIT_AUTHOR%" >>%FILE%
echo #endif _BUILD_VERSION_H >>%FILE%
exit /b

:EXIT

